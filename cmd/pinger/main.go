/* A command line tool for collecting ICMP ping stats and storing into an Influx database */

package main

import (
	"flag"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-ping/ping"
	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/influx"
	"gitlab.com/mergetb/exptools/forensics/measurement/pkg/sampler"
	"gitlab.com/mergetb/exptools/forensics/pinger/pkg/config"
)

func main() {
	filename := flag.String("f", "", "config file")
	flag.Parse()

	if *filename == "" {
		log.Fatal("config file must be provided on the command line")
	}
	cfg, err := config.GetConfigFromYaml(*filename)
	if err != nil {
		log.Fatal(err)
	}

	db, err := influx.NewDatabase(cfg.InfluxServer, cfg.InfluxDatabase)
	if err != nil {
		log.Fatal(err)
	}

	// start channel writer
	go db.WriteChannel()

	src := getHostname()

	pinger, err := ping.NewPinger(cfg.Host)
	if err != nil {
		log.Fatal(err)
	}
	pinger.OnRecv = func(pkt *ping.Packet) {
		log.Printf("packet received rtt=%d", pkt.Rtt)
		pt := &sampler.Point{
			Name: "ping",
			Tags: map[string]string{
				"src":  src,
				"dst":  cfg.Host,
				"type": "recv",
			},
			Fields: map[string]interface{}{
				"rtt":  pkt.Rtt,
				"ttl":  pkt.Ttl,
				"size": pkt.Nbytes,
				"seq":  pkt.Seq,
			},
			Timestamp: time.Now(),
		}
		db.Channel <- pt
	}
	// pinger.OnDuplicateRecv = func(pkt *ping.Packet) {
	// }
	pinger.OnFinish = func(stats *ping.Statistics) {
		log.Printf("stats avg rtt=%d loss=%f", stats.AvgRtt, stats.PacketLoss)
		pt := &sampler.Point{
			Name: "ping",
			Tags: map[string]string{
				"src":  src,
				"dst":  cfg.Host,
				"type": "stats",
			},
			Fields: map[string]interface{}{
				"sent":      stats.PacketsSent,
				"recv":      stats.PacketsRecv,
				"dupes":     stats.PacketsRecvDuplicates,
				"loss":      stats.PacketLoss,
				"minrtt":    stats.MinRtt,
				"avgrtt":    stats.AvgRtt,
				"maxrtt":    stats.MaxRtt,
				"stddevrtt": stats.StdDevRtt,
			},
			Timestamp: time.Now(),
		}
		db.Channel <- pt
	}
	if cfg.Count > 0 {
		pinger.Count = cfg.Count
	}
	if cfg.Size > 0 {
		pinger.Size = cfg.Size
	}
	if cfg.Interval > 0 {
		pinger.Interval = cfg.Interval
	}
	if cfg.Ttl > 0 {
		pinger.TTL = cfg.Ttl
	}
	pinger.SetPrivileged(true)
	err = pinger.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func getHostname() string {
	fqdn, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}
	return strings.Split(fqdn, ".")[0]
}
