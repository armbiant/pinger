GO=go

all: build/pinger

.PHONY: clean
clean:
	rm -rf build

build/pinger: cmd/pinger/*.go
	CGO_ENABLED=0 $(GO) build -o $@ cmd/pinger/*.go
